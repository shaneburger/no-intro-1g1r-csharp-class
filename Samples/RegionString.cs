﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sample_RegionList
{
    class Program
    {
        static void Main(string[] args)
        {
            clsDatabase RegionDB;

            Console.WriteLine("No-Intro 1G1R DAT File Manipulator - Region List");

            if(args.Length > 0)
            {
                if (File.Exists(args[0]))
                {
                    RegionDB = new clsDatabase(args[0]);
                    Console.WriteLine("Valid Regions: " + RegionDB.getRegionString());

                }
                else
                {
                    Console.WriteLine("Please add a No-Intro Dat file to process as an Argument.");
                }
            }
            else
            {
                Console.WriteLine("Please add a No-Intro Dat file to process as an Argument.");
            }
        }
    }
}
