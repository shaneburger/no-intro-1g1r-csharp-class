/* 
 * This file is part of the my GitLab Repository.
 * Copyright (c) 2019 Shane Burger.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
* 
* No-Intro 1G1R DAT File Manipulator 
* 
*   getDictionary - Get the Database in a Dictionary Format
*   
*       Dictionary<string,List<string>>
*       
*       string       = Parent Rom Name without Extension (and no REGION Identifier)
*                      ie./ 
*                           Wonder Boy III - The Dragon's Trap (USA, Europe)
*                      
*       List<string> = A list of Regions Rom Title with REGION identifier at the end, seperated by a | character
*                      ie./ 
*                           Wonder Boy III - The Dragon's Trap (USA, Europe)|EUR
*		                    Wonder Boy III - The Dragon's Trap (USA, Europe)|USA
*                           Turma da Monica em O Resgate (Brazil)|BRA
*   
*   
*   getRegionList - Get the Valid Regions per DAT file (List)
*   
*     List<string>   = List of REGION identifiers.
*                      ie./
*                          ASI
*                          AUS
*                          BRA
*                          EUR
*                          JPN
*                          KOR
*                          TAI
*                          USA
*                          
*                          
*    getRegionString - Get the Valid Regions per DAT file (String)
*    
*             string = string of REGION identifiers seperated by a Comma (,)
*                      ie./
*                          ASI,AUS,BRA,EUR,JPN,KOR,TAI,USA
*                          
*                           
*   get1G1RList - Get a List of rom files in 1G1R Format using Regions
* 
*     List<string>   = List of Rom Name without Extension (and no REGION Identifier)
*                      But this uses an input (comma seperated) string of regions.
*                      The region priority is first highest, last lowest. (Similar to sorting Region priority in clrmamepro.
*                      I have tested this function against the same config in clrmamepro and its output pretty much identical
*       
*       *Optional*  
*       
*    Bool bCleanMode = If set to False, it gives 100% of the List
*                    = If set to  True, it will try to remove all non-game/non-official/etc so you get a Clean list of playable games.
*                    
*                    
*   getTotalGames - Get Total Amount of Games in last used Operation
*   
*                int = Provides an integer, that Total's the amount of Games that were processed in the last operation (Full List,1G1R etc)
* 
* 
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using RestSharp.Contrib;

class clsDatabase
{

    Dictionary<string, List<string>> dictGames;
    int intTotalGames;

    public clsDatabase(string strDatFile)
    {

        string[] listDatLines = File.ReadAllLines(strDatFile);

        dictGames = new Dictionary<string, List<string>>();
        intTotalGames = 0;

        for (int i = 0; i < listDatLines.Length; i++)
        {
            string strLine = HttpUtility.HtmlDecode(listDatLines[i].Replace("\t", string.Empty).Trim());

            if ((strLine.StartsWith("<game name=")) && (strLine.Contains("cloneof=") == false))
            {
                // Parent, get Releases                    

                bool bLoop = true;
                int j = i;

                while (bLoop)
                {
                    string strSubLine = HttpUtility.HtmlDecode(listDatLines[j].Replace("\t", string.Empty).Trim());

                    if (strSubLine.StartsWith("<release name="))
                    {
                        string strName = getReleaseName(strSubLine);
                        string strRegion = getReleaseRegion(strSubLine);

                        if (dictGames.ContainsKey(strName))
                        {

                            dictGames[strName].Add(strName + "|" + strRegion);
                            intTotalGames++;
                        }
                        else
                        {

                            List<string> listTemp = new List<string>();
                            listTemp.Add(strName + "|" + strRegion);
                            dictGames.Add(strName, listTemp);
                            intTotalGames++;
                        }
                    }

                    if (strSubLine.Equals("</game>"))
                    {
                        bLoop = false;
                    }

                    j++;
                }
            }

            if ((strLine.StartsWith("<game name=")) && (strLine.Contains("cloneof=") == true))
            {

                string strParentName = isClone_GetParentName(strLine);
                string strCloneName = isClone_GetCloneName(strLine);
                List<string> listReleases = new List<string>();

                bool bLoop = true;
                int j = i;

                while (bLoop)
                {
                    string strSubLine = HttpUtility.HtmlDecode(listDatLines[j].Replace("\t", string.Empty).Trim());

                    if (strSubLine.StartsWith("<release name="))
                    {
                        string strName = getReleaseName(strSubLine);
                        string strRegion = getReleaseRegion(strSubLine);
                        listReleases.Add(strName + "|" + strRegion);
                    }

                    if (strSubLine.Equals("</game>"))
                    {
                        bLoop = false;
                    }

                    j++;
                }

                if (dictGames.ContainsKey(strParentName))
                {
                    foreach (string strAdd in listReleases)
                    {
                        dictGames[strParentName].Add(strAdd);
                        intTotalGames++;
                    }
                }
                else
                {
                    dictGames.Add(strParentName, listReleases);
                    intTotalGames++;
                }
            }
        }
    }

    public Dictionary<string, List<string>> getDictionary()
    {
        return dictGames;
    }

    public List<string> get1G1RList(string strRegions, bool bCleanMode)
    {
        List<string> listRegions = convCSVtoList(strRegions);

        List<string> listClean = new List<string> { "(Unl", "[BIOS", "(Proto", "(Program", "(Sample", "(Demo", "[b]", "(Promo", "(Kiosk", "(Test", "(SDK", "(Beta" };

        List<string> listOut = new List<string>();

        foreach (KeyValuePair<string, List<string>> kvPair in dictGames)
        {
            List<string> listRoms = kvPair.Value;
            string strKey = kvPair.Key;
            bool bFound = false;
            intTotalGames = 0;

            foreach (string strUserRegion in listRegions)
            {
                if (bFound == false)
                {
                    foreach (string strRom in listRoms)
                    {
                        if (bFound == false)
                        {
                            if (strRom.EndsWith(strUserRegion))
                            {
                                string strRegion = strRom.Split('|')[1];
                                string strFilename = strRom.Split('|')[0];

                                if (bCleanMode == true)
                                {
                                    bool bAdd = true;
                                    foreach (string strCleanTag in listClean)
                                    {
                                        if (strFilename.Contains(strCleanTag))
                                        {
                                            bAdd = false;
                                        }
                                    }

                                    if (bAdd == true)
                                    {
                                        listOut.Add(strFilename);
                                        intTotalGames++;
                                    }
                                }
                                else
                                {
                                    listOut.Add(strFilename);
                                    intTotalGames++;
                                }
                                bFound = true;
                            }
                        }
                    }
                }
            }
        }

        listOut.Sort();

        return listOut;
    }

    public List<string> getClones(string strParent)
    {
        List<string> listOut = new List<string>();

        foreach (string strClone in dictGames[strParent]) 
        {
            Console.WriteLine(strClone);
        }

        return listOut;
    }

    public List<string> getRegionList()
    {
        List<string> listRegions = new List<string>();

        foreach (KeyValuePair<string, List<string>> kvPair in dictGames)
        {
            List<string> listRoms = kvPair.Value;

            foreach (string strRom in listRoms)
            {
                string strRegion = strRom.Split('|')[1];

                if (listRegions.Contains(strRegion) == false)
                {
                    listRegions.Add(strRegion);
                }
            }
        }

        listRegions.Sort();

        return listRegions;
    }

    public string getRegionString()
    {
        List<string> listRegions = getRegionList();

        StringBuilder sbOut = new StringBuilder();

        foreach (string strRegion in listRegions)
        {
            sbOut.Append(strRegion + ",");
        }

        return sbOut.ToString().TrimEnd(',');
    }

    public int getTotalGames()
    {
        return intTotalGames;
    }

    public List<string> convCSVtoList(string strCSV)
    {
        List<string> listOut = new List<string>();

        foreach (string strPart in strCSV.Split(','))
        {
            listOut.Add(strPart);

        }

        return listOut;
    }

    private string isClone_GetCloneName(string strLine)
    {
        string[] arrLineParts = strLine.Split('\"');
        string strCloneName = string.Empty;

        for (int i = 0; i < arrLineParts.Length; i++)
        {
            if (arrLineParts[i].Contains("<game name="))
            {
                strCloneName = arrLineParts[i + 1];
            }
        }
        return strCloneName;
    }

    private string isClone_GetParentName(string strLine)
    {
        string[] arrLineParts = strLine.Split('\"');
        string strCloneName = string.Empty;

        for (int i = 0; i < arrLineParts.Length; i++)
        {
            if (arrLineParts[i].Contains("cloneof="))
            {
                strCloneName = arrLineParts[i + 1];
            }
        }
        return strCloneName;
    }

    private string isParent_GetParentName(string strLine)
    {
        string[] arrLineParts = strLine.Split('\"');
        string strCloneName = string.Empty;

        for (int i = 0; i < arrLineParts.Length; i++)
        {
            if (arrLineParts[i].Contains("<game name="))
            {
                strCloneName = arrLineParts[i + 1];
            }
        }
        return strCloneName;
    }

    private string getReleaseName(string strLine)
    {
        string[] arrLineParts = strLine.Split('\"');
        string strReleaseName = string.Empty;

        for (int i = 0; i < arrLineParts.Length; i++)
        {
            if (arrLineParts[i].Contains("<release name="))
            {
                strReleaseName = arrLineParts[i + 1];
            }
        }

        return strReleaseName;
    }

    private string getReleaseRegion(string strLine)
    {
        string[] arrLineParts = strLine.Split('\"');
        string strRegion = string.Empty;

        for (int i = 0; i < arrLineParts.Length; i++)
        {
            if (arrLineParts[i].Contains("region="))
            {
                strRegion = arrLineParts[i + 1];
            }
        }

        return strRegion;
    }

}