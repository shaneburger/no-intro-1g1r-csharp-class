# No-Intro 1G1R DAT File Manipulator
 (Dotnet 2.0, Linux/Mono Compatible)
 
## A Library to help with the creation of 1G1R romsets.
 
  Notes;    - Ensure your Romset's are complete and up-to-date with the DAT you are using.
            - Ensure your DAT has a Parent/Clone relationship (For Example, Atari 2600 is incomplete and will only list about 1/3 the roms in 1G1R mode, As of June 2019).
            - I have matched the output of this library with clrmamepro and its close to, if not, 100% in my tests, if you find any bugs or issues please submit them!
 
   **getDictionary** - Get the Database in a Dictionary Format
   
       Dictionary<string,List<string>>
       
       string       = Parent Rom Name without Extension (and no REGION Identifier)
                      ie./ 
                           Wonder Boy III - The Dragon's Trap (USA, Europe)
                      
       List<string> = A list of Regions Rom Title with REGION identifier at the end, seperated by a | character
                      ie./ 
                           Wonder Boy III - The Dragon's Trap (USA, Europe)|EUR
		                   Wonder Boy III - The Dragon's Trap (USA, Europe)|USA
                           Turma da Monica em O Resgate (Brazil)|BRA
   
   
   **getRegionList** - Get the Valid Regions per DAT file (List)
   
     List<string>   = List of REGION identifiers.
                      ie./
                          ASI
                          AUS
                          BRA
                          EUR
                          JPN
                          KOR
                          TAI
                          USA
                          
                          
**getRegionString** - Get the Valid Regions per DAT file (String)
    
             string = string of REGION identifiers seperated by a Comma (,)
                      ie./
                          ASI,AUS,BRA,EUR,JPN,KOR,TAI,USA
                          
                           
   **get1G1RList** - Get a List of rom files in 1G1R Format using Regions
 
     List<string>   = List of Rom Name without Extension (and no REGION Identifier)
                      But this uses an input (comma seperated) string of regions.
                      The region priority is first highest, last lowest. (Similar to sorting Region priority in clrmamepro.
                      I have tested this function against the same config in clrmamepro and its output pretty much identical
       
       _Optional_  
       
    Bool bCleanMode = If set to False, it gives 100% of the List
                    = If set to  True, it will try to remove all non-game/non-official/etc so you get a Clean list of playable games.
                    
                    
   **getTotalGames** - Get Total Amount of Games in last used Operation
   
                int = Provides an integer, that Total's the amount of Games that were processed in the last operation (Full List,1G1R etc)
 
 